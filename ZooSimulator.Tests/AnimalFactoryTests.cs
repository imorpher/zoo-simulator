﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooSimulator.Models;

namespace ZooSimulator.Tests
{
    [TestClass]
    public class AnimalFactoryTests
    {
        private void CheckTypesAndQuantity(List<Animal> animals, int quantity, Type type)
        {
            int count = 0;
            foreach (Animal animal in animals)
            {
                Assert.AreEqual(type, animal.GetType());
                count++;
            }
            Assert.AreEqual(quantity, count);
        }


        [TestMethod]
        public void GetStartingAnimals_MonkeyQuantity5_Returns5Monkeys()
        {
            CheckTypesAndQuantity(AnimalFactory.GetAnimals(AnimalType.Monkey, 5).ToList(), 5, typeof(Monkey));
        }

        [TestMethod]
        public void GetStartingAnimalss_ElephantQuantity3_Returns3Elephants()
        {
            CheckTypesAndQuantity(AnimalFactory.GetAnimals(AnimalType.Elephant, 3).ToList(), 3, typeof(Elephant));
        }

        [TestMethod]
        public void GetStartingAnimals_GiraffeQuantity1_Returns1Giraffes()
        {
            CheckTypesAndQuantity(AnimalFactory.GetAnimals(AnimalType.Monkey, 1).ToList(), 1, typeof(Monkey));
        }
    }
}
