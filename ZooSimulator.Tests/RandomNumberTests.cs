﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZooSimulator.Tests
{
    [TestClass]
    public class RandomNumberTests
    {
        [TestMethod]
        public void GetRandomDouble_NormalValues_ReturnIsWithinRange()
        {
            double min = 50;
            double max = 100;

            double randomNumber = RandomNumber.GetRandomDouble(min, max);

            Assert.IsTrue(randomNumber > min);
            Assert.IsTrue(randomNumber < max);
        }

        [TestMethod]
        public void GetRandomDouble_NormalValues_ReturnsRandom()
        {
            double randomNumber1 = RandomNumber.GetRandomDouble(50, 100);
            double randomNumber2 = RandomNumber.GetRandomDouble(50, 100);

            Assert.AreNotEqual(randomNumber1, randomNumber2);
        }
    }
}
