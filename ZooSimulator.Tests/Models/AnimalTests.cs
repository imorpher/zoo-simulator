﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooSimulator.Models;

namespace ZooSimulator.Tests.Models
{
    [TestClass]
    public class AnimalTests
    {
        [TestMethod]
        public void Age_Inputdouble_DecreasesHealthByValue()
        {
            Animal elephant = new Elephant();            
            elephant.Age(10);
            Assert.AreEqual(90, elephant.Health);
        }

        [TestMethod]
        public void Age_CalledMultipleTimes_DecreasesHealthByValue()
        {
            Animal elephant = new Elephant();
            elephant.Age(10);
            elephant.Age(10);
            Assert.AreEqual(80, elephant.Health);
        }

        [TestMethod]
        public void Age_GreaterThanCurrentHealth_DecreasesHealthAndCapsAtZero()
        {
            Animal elephant = new Elephant();
            elephant.Age(110);
            Assert.AreEqual(0, elephant.Health);
        }

        [TestMethod]
        public void Feed_InputLessThanHealthMissing_IncreasesHealthByValue()
        {
            Animal elephant = new Elephant();

            elephant.Age(20);

            elephant.Feed(10);

            Assert.AreEqual(90, elephant.Health);
        }
        
        [TestMethod]
        public void Feed_InputGreaterThanHealthMissing_CapsAt100()
        {
            Animal elephant = new Elephant();

            elephant.Age(10);

            elephant.Feed(25);

            Assert.AreEqual(100, elephant.Health);
        }

        [TestMethod]
        public void Feed_AtMaximumHealthAlready_CapsAt100()
        {
            Animal elephant = new Elephant();
            
            elephant.Feed(25);

            Assert.AreEqual(100, elephant.Health);
        }

        [TestMethod]
        public void CheckHealth_HasHealthGreaterThanMinumum_IsNotDead()
        {
            Animal monkey = new Monkey();
            monkey.Age(10);

            Assert.AreEqual(false, monkey.Dead);
        }

        [TestMethod]
        public void CheckHealth_HasHealthLessThanMinumum_IsDead()
        {
            Animal monkey = new Monkey();
            monkey.Age(90);

            Assert.AreEqual(true, monkey.Dead);
        }
    }
}
