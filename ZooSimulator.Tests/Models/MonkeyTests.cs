using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooSimulator.Models;

namespace ZooSimulator.Tests.Models
{
    [TestClass]
    public class MonkeyTests
    {
        [TestMethod]
        public void Constructor_SetsType()
        {
            Animal monkey = new Monkey();

            Assert.AreEqual(AnimalType.Monkey, monkey.AnimalType);
        }

        [TestMethod]
        public void CheckHealth_HasHealthLessThanMinimum_IsDead()
        {
            Monkey monkey = new Monkey();
            monkey.Age(71);
            monkey.CheckHealth();

            Assert.AreEqual(true, monkey.Dead);
        }
    }
}