using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooSimulator.Models;

namespace ZooSimulator.Tests.Models
{
    [TestClass]
    public class GiraffeTests
    {
        [TestMethod]
        public void Constructor_SetsType()
        {
            Animal giraffe = new Giraffe();

            Assert.AreEqual(AnimalType.Giraffe, giraffe.AnimalType);
        }

        [TestMethod]
        public void CheckHealth_HasHealthLessThanMinimum_IsDead()
        {
            Giraffe girafee = new Giraffe();
            girafee.Age(51);
            girafee.CheckHealth();

            Assert.AreEqual(true, girafee.Dead);
        }
    }
}