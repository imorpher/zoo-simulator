﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooSimulator.Models;

namespace ZooSimulator.Tests.Models
{
    [TestClass]
    public class ZooTests
    {
        private List<Animal> GetStartingAnimals()
        {
            List<Animal> startingAnimals = new List<Animal>();
            startingAnimals.AddRange(AnimalFactory.GetAnimals(AnimalType.Giraffe, 5));
            startingAnimals.AddRange(AnimalFactory.GetAnimals(AnimalType.Elephant, 5));
            startingAnimals.AddRange(AnimalFactory.GetAnimals(AnimalType.Monkey, 5));

            return startingAnimals;
        }

        [TestMethod]
        public void AgeAnimals_SingleAnimal_HealthDecreases()
        {
            IEnumerable<Animal> startingAnimals = new List<Animal>()
            {
                new Elephant()
            };

            Zoo zoo = new Zoo(startingAnimals);

            double beforeHealth = zoo.Animals.First().Health;

            zoo.AgeAnimals();

            Assert.AreNotEqual(beforeHealth, zoo.Animals.First().Health);
        }

        [TestMethod]
        public void AgeAnimals_AllAnimals_HealthDecreases()
        {
            Zoo zoo = new Zoo(GetStartingAnimals());

            zoo.AgeAnimals();

            Assert.IsTrue(!zoo.Animals.Any(a => a.Health == 100));
        }

        [TestMethod]
        public void FeedAnimals_SingleAnimal_HealthIncreases()
        {
            IEnumerable<Animal> startingAnimals = new List<Animal>()
            {
                new Elephant()
            };

            Zoo zoo = new Zoo(startingAnimals);

            zoo.AgeAnimals();

            double beforeHealth = zoo.Animals.First().Health;

            zoo.FeedAnimals();

            Assert.AreNotEqual(beforeHealth, zoo.Animals.First().Health);
        }

        [TestMethod]
        public void FeedAnimals_AllAnimals_HealthIncreases()
        {
            Zoo zoo = new Zoo(GetStartingAnimals());

            zoo.AgeAnimals();

            List<double> animalsHealthBefore = new List<double>(zoo.Animals.Select(a => a.Health));

            zoo.FeedAnimals();

            for (int count = 0; count < animalsHealthBefore.Count; count++)
            {
                Assert.AreNotEqual(animalsHealthBefore[count], zoo.Animals.ToList()[count].Health);
            }
        }
    }
}
