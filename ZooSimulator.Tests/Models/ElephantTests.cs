using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooSimulator.Models;

namespace ZooSimulator.Tests.Models
{
    [TestClass]
    public class ElephantTests
    {
        [TestMethod]
        public void Constructor_SetsType()
        {
            Animal elephant = new Elephant();

            Assert.AreEqual(AnimalType.Elephant, elephant.AnimalType);
        }

        [TestMethod]
        public void CheckHealth_HasHealthLessThanMinimumOnFirstPass_IsInjuredAndIsNotDead()
        {
            Elephant elephant = new Elephant();
            elephant.Age(31);

            Assert.AreEqual(false, elephant.Dead);
            Assert.AreEqual(true, elephant.Injured);
        }

        [TestMethod]
        public void CheckHealth_HasHealthLessThanMinimumOnSecondPass_IsDead()
        {
            Elephant elephant = new Elephant();
            elephant.Age(31);
            elephant.Age(10);

            Assert.AreEqual(true, elephant.Dead);
        }

        [TestMethod]
        public void CheckHealth_HasHealthMoreThanMinimumOnSecondPass_NotInjuredAndNotDead()
        {
            Elephant elephant = new Elephant();
            elephant.Age(31);

            Assert.AreEqual(false, elephant.Dead);
            Assert.AreEqual(true, elephant.Injured);

            elephant.Feed(100);

            elephant.Age(1);

            Assert.AreEqual(false, elephant.Injured);
            Assert.AreEqual(false, elephant.Dead);
        }

        [TestMethod]
        public void CheckHealth_IsFedAndStillLessThanMinimumWithOnlyOneAge_IsInjuredAndNotDead()
        {
            Elephant elephant = new Elephant();
            elephant.Age(50);

            Assert.AreEqual(false, elephant.Dead);
            Assert.AreEqual(true, elephant.Injured);

            elephant.Feed(1);

            Assert.AreEqual(true, elephant.Injured);
            Assert.AreEqual(false, elephant.Dead);
        }

        [TestMethod]
        public void CheckHealth_IsFedAboveMinimumAndAgesBelowMinimum_IsInjuredAndNotDead()
        {
            Elephant elephant = new Elephant();
            elephant.Age(50);

            Assert.AreEqual(false, elephant.Dead);
            Assert.AreEqual(true, elephant.Injured);

            elephant.Feed(100);

            Assert.AreEqual(false, elephant.Injured);
            Assert.AreEqual(false, elephant.Dead);

            elephant.Age(50);

            Assert.AreEqual(false, elephant.Dead);
            Assert.AreEqual(true, elephant.Injured);
        }
    }
}