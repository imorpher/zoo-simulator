﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooSimulator.Models;

namespace ZooSimulator.Tests.Models
{
    [TestClass]
    public class SimulatorTests
    {
        [TestMethod]
        public void Initializer_CreatesZoo()
        {
            Assert.IsTrue(Simulator.Instance.Zoo.Animals.Any());
        }
        
        [TestMethod]
        public void IncreaseTime_CalledOnce_HoursIncreaseAndAnimalsAge()
        {
            Simulator simulator = Simulator.Instance;

            int hours = simulator.Hours;

            simulator.IncreaseTime();

            Assert.IsTrue(simulator.Hours == hours + 1);

            Assert.IsTrue(!simulator.Zoo.Animals.Any(a => a.Health == 100));

        }

        [TestMethod]
        public void IncreaseTime_CalledTwice_HoursIncreaseAndAnimalsAge()
        {
            Simulator simulator = Simulator.Instance;

            int hours = simulator.Hours;

            simulator.IncreaseTime();
            simulator.IncreaseTime();

            Assert.IsTrue(simulator.Hours == hours + 2);

            Assert.IsTrue(!simulator.Zoo.Animals.Any(a => a.Health == 100));

        }

        [TestMethod]
        public void IncreaseTime_NoAnimalsAlive_DoesNotAddHours()
        {
            Simulator simulator = Simulator.Instance;

            while (simulator.Zoo.AnimalsAlive.Any())
            {
                simulator.IncreaseTime();
            }

            int hours = simulator.Hours;

            simulator.IncreaseTime();

            Assert.AreEqual(hours, simulator.Hours);
        }

        [TestMethod]
        public void Restart_CreatesNewZooAndResetsHours()
        {
            Simulator simulator = Simulator.Instance;

            simulator.IncreaseTime();
            simulator.IncreaseTime();
            simulator.IncreaseTime();
            simulator.IncreaseTime();

            Zoo zoo = simulator.Zoo;
            int hours = simulator.Hours;

            simulator.Restart();

            Assert.AreNotSame(zoo, simulator.Zoo);
            Assert.AreNotEqual(hours, simulator.Hours);
        }
    }
}
