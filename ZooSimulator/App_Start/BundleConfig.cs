﻿using System.Web.Optimization;

namespace ZooSimulator
{
    public static class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/simulator").Include(
                        "~/Scripts/jquery-2.2.3.js",
                        "~/Scripts/simulator.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/custom.css"));
        }
    }
}
