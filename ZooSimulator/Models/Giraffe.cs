﻿namespace ZooSimulator.Models
{
    public class Giraffe : Animal
    {
        private const double MinHealth = 50;

        public Giraffe() : base(AnimalType.Giraffe, MinHealth)
        {
        }
    }
}