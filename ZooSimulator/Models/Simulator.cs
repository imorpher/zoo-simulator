﻿using System.Collections.Generic;
using System.Linq;

namespace ZooSimulator.Models
{
    public class Simulator
    {
        public Zoo Zoo { get; private set; }
    
        public int Hours { get; private set; }

        public static Simulator Instance { get; } = new Simulator();

        private Simulator()
        {
            Zoo = new Zoo(GetStartingAnimals());            
        }

        public void IncreaseTime()
        {
            if (!Zoo.AnimalsAlive.Any())
                return;
            Zoo.AgeAnimals();
            Hours++;
        }

        public void Restart()
        {
            Zoo = new Zoo(GetStartingAnimals());
            Hours = 0;
        }

        private List<Animal> GetStartingAnimals()
        {
            List<Animal> startingAnimals = new List<Animal>();
            startingAnimals.AddRange(AnimalFactory.GetAnimals(AnimalType.Giraffe, 5));
            startingAnimals.AddRange(AnimalFactory.GetAnimals(AnimalType.Elephant, 5));
            startingAnimals.AddRange(AnimalFactory.GetAnimals(AnimalType.Monkey, 5));

            return startingAnimals;
        }
    }
}