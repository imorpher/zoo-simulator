namespace ZooSimulator.Models
{
    public class Monkey : Animal
    {
        private const double MinHealth = 30;

        public Monkey() : base(AnimalType.Monkey, MinHealth)
        {
        }
    }
}