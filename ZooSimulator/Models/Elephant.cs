﻿namespace ZooSimulator.Models
{
    public class Elephant : Animal
    {
        private const double MinHealth = 70;

        public Elephant() : base(AnimalType.Elephant, MinHealth)
        {
        }

        public override void Feed(double health)
        {
            base.Feed(health);
            if (Injured && Health > MinHealth)
                Injured = false;
        }

        public override void CheckHealth()
        {
            if (Dead) return;

            if (Health < MinimumHealth)
            {
                Dead = Injured;
                Injured = true;
            }
            else
                Injured = false;
        }
    }
}