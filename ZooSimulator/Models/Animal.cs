﻿namespace ZooSimulator.Models
{
    public abstract class Animal
    {
        private const double MaxHealth = 100;
        protected readonly double MinimumHealth;

        public double Health { get; private set; } = MaxHealth;

        public AnimalType AnimalType { get; private set; }

        public bool Dead { get; protected set; }
        
        public bool Injured { get; protected set; }

        protected Animal(AnimalType animalType, double minimumHealth)
        {
            AnimalType = animalType;
            MinimumHealth = minimumHealth;
        }
        
        public void Age(double health)
        {
            Health = health > Health ? 0 : Health - health;
            CheckHealth();
        }

        public virtual void Feed(double health)
        {
            Health = (Health + health) > MaxHealth ? MaxHealth : Health + health; 
        }
        
        public virtual void CheckHealth()
        {
            if (!Dead && Health < MinimumHealth)
                Dead = true;
        }
    }
}