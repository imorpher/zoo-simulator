namespace ZooSimulator.Models
{
    /// <summary>
    /// Types of Animals.
    /// </summary>
    public enum AnimalType
    {
        Elephant,
        Giraffe,
        Monkey
    }
}