﻿using System.Collections.Generic;
using System.Linq;

namespace ZooSimulator.Models
{
    public class Zoo
    {
        public IEnumerable<Animal> Animals { get; }

        public IEnumerable<Animal> AnimalsAlive => Animals.Where(a => !a.Dead);

        public Zoo(IEnumerable<Animal> startingAnimals)
        {
            Animals = startingAnimals;
        }

        public void FeedAnimals()
        {
            List<AnimalType> animalTypes = new List<AnimalType>();
            foreach (AnimalType animalType in AnimalsAlive.Select(a => a.AnimalType))
            {
                if (!animalTypes.Contains(animalType))
                    animalTypes.Add(animalType);
            }

            foreach (AnimalType animalType in animalTypes)
            {
                double feedAmount = RandomNumber.GetRandomDouble(10, 25);
                AnimalsAlive.Where(a => a.AnimalType == animalType).ToList().ForEach(a => a.Feed(feedAmount));
            }
        }

        public void AgeAnimals()
        {
            foreach (Animal animal in AnimalsAlive)
            {
                animal.Age(RandomNumber.GetRandomDouble(0, 20));
            }
        }
    }
}