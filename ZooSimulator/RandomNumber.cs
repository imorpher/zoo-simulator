﻿using System;

namespace ZooSimulator
{
    public static class RandomNumber
    {
        private static Random _random;

        public static double GetRandomDouble(double minimum, double maximum)
        {
            // Lazy loading.
            if (_random == null) 
                _random = new Random();

            return _random.NextDouble()*(maximum - minimum) + minimum;
        }
    }
}