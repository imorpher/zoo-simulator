﻿======= Approach taken =======

I took a TDD approach to this and used NCrunch through the development process to leverage
it's automated test runner. For each method/class I followed the workflow that Rich suggested on 
Wednesday of "Red, Green, Refactor" as much as possible.

The front end is fairly simple as I wasn't sure how much detail you wanted in this regards so it is just
a simple bootstrap site with some ajax calls to the server to provide the necessary functionality. I
added a restart button the UI which wasn't noted down but I felt like was useful.

The server should be under full unit test with full code coverage and well named unit tests, again following 
the pattern I believe Rich suggested.

======= Design choices =======

I created an abstract Animal class of which Elephant/Giraffe/Monkey were all derived classes of. The idea
here was to try and minimise code reputation so I had a central place I could implement the relevant methods
which were shared across multiple, as well as be able to override that implementation where need be (e.g. 
Elephants method to check health was different). This also allowed me to treat a collection of these 
as a collection of Animal rather than separate collections, and any references wouldn't need to care about
it's underlying type as long as it was a derived type of Animal. This should also make extensibility 
in the future easier for implementing more animals.

Another model "Zoo" holds the relevant data/methods for a zoo object. It holds a Collection of animals
as well as allows for the ability to feed and age the animals in the zoo as this needs has to be done to all
animals in the zoo rather than on an individual level. Having it here helps prevent potential repeated code.
The constructor takes a collection of Animals so that if needed - implementing a new zoo with a different set
of animals is possible.

The main class which holds the state of the current simulation in progress is Simulator which holds the zoo
and hours elapsed. This is a singleton as there should be only one simulation active at one time. 

Creation of the list of animals for the zoo is done through a simple factory that will build a collection 
of a certain type of animal, which are then sent through the Zoo constructor. 

A controller has the 3 following methods :
	- Feed animals : Which feeds all animals in the zoo with a random value depending on their type. This is called
						from a button press. Partial view for the Simulator refreshes.
	- Age zoo : All animals in the zoo lose some health and the hours increase by one. This is called from a set
					interval every 20 seconds automatically. Partial view for simulator refreshes.
	- Restart zoo : Creation of a new set of animals and hours Reset to 0. Partial view for simulator refreshes.


I would have liked to have moved the timer to the server side and use websockets to keep the UI up to date but I 
thought this might go out of bounds of the "simple application" requested.