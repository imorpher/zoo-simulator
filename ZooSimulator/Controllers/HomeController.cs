﻿using System.Web.Mvc;
using ZooSimulator.Models;

namespace ZooSimulator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult AgeZoo()
        {
            Simulator.Instance.IncreaseTime();
            return PartialView("Simulator", Simulator.Instance);
        }
        
        public ActionResult FeedZoo()
        {
            Simulator.Instance.Zoo.FeedAnimals();
            return PartialView("Simulator", Simulator.Instance);
        }
        
        public ActionResult Restart()
        {
            Simulator.Instance.Restart();
            return PartialView("Simulator", Simulator.Instance);
        }
        
    }
}