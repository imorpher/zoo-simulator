﻿using System;
using System.Collections.Generic;
using ZooSimulator.Models;

namespace ZooSimulator
{
    public static class AnimalFactory
    {
        public static IEnumerable<Animal> GetAnimals(AnimalType type, int quantity)
        {
            List<Animal> animals = new List<Animal>();

            for (int count = 0; count < quantity; count++)
            {
                switch (type)
                {
                    case AnimalType.Elephant:
                        animals.Add(new Elephant());
                        break;
                    case AnimalType.Giraffe:
                        animals.Add(new Giraffe());
                        break;
                    case AnimalType.Monkey:
                        animals.Add(new Monkey());
                        break;
                    default:
                        // This line isn't unit tested. Unsure on how to without adding unneccessary enum value.
                        throw new ArgumentOutOfRangeException(nameof(type), type, null);
                }
            }

            return animals;
        }
    }
}