﻿$(function () {
    var serverPost = function (url) {
        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            success: function (data) {
                $("#simulator").html(data);
            },
            error: function (xhr) {
                console.log(xhr.responseText);
            }
        });
    }

    $(document)
        .ready(function () {
            $('#feed').click(function () {
                serverPost('/Home/FeedZoo');
            });

            $('#restart').click(function () {
                serverPost('/Home/Restart');
            });

            setInterval(function () { serverPost('/Home/AgeZoo'); }, 20000);    //20 Seconds.
        });
});